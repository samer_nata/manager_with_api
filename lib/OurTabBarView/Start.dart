import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:manager_finish/Models/Event.dart';
import 'package:manager_finish/Models/Race.dart';

import 'package:manager_finish/Screen/RaceInfo.dart';


import 'package:toast/toast.dart';

import '../Database.dart';

class Start extends StatefulWidget {
  Event_l event_l;

  Start(this.event_l);

  @override
  _StartState createState() => _StartState(event_l);
}

class _StartState extends State<Start> {

  Event_l event_l;
  ScrollController _scrollController;

  _StartState(this.event_l);

  @override
  void initState() {
    _scrollController = ScrollController();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Container(child: racesListView());
  }



  FutureBuilder<List<Race>> racesListView() {
    return FutureBuilder(
      initialData: [],
      future: DBProvider.db.Race_list_all(event_l),
      builder: (BuildContext context, AsyncSnapshot<List<Race>> snapshot) {
//        print(event_l.toString());
        if (snapshot.hasData && snapshot.data.length > 0) {
          snapshot.data.sort((a, b) => a.startTimeMs.compareTo(b.startTimeMs));
          return ListView.builder(
              itemCount: snapshot.data.length,
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                Race item = snapshot.data[index];

                return Padding(
                  padding: const EdgeInsets.all(1.0),
                    child: Container(
                    color: Color(0xff121214),
                    height: 90,
                    child: Stack(
                      children: <Widget>[
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 0.0, top: 2.0),
                                    child: Container(
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            width: 60,
                                            child: InkWell(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    new MaterialPageRoute(
                                                        builder: (context) =>
                                                            RaceInfo(event_l,
                                                                item)));
                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.only(left: 2),
                                                child: Text(
                                                  item.name,
                                                  style: TextStyle(
                                                      decoration: TextDecoration
                                                          .underline,
                                                      color: Colors.white,
                                                      fontSize: 15,
                                                      fontWeight:
                                                          FontWeight.w100),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Text(
                                            "Start ${item.startTimeMs.toString().substring(10, 16)}",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15,
                                                fontWeight: FontWeight.w100),
                                          ),
//
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                              padding: const EdgeInsets.only(right: 16.0),
                              child: item.is_start
                                  ? Text(
                                      "Tävlingar har startat",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w100),
                                    )
                                  : InkWell(
                                      onTap: () => startRace(item),
                                      child: Container(
                                        child: Center(
                                          child: Text("Start",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w100)),
                                        ),
                                        width: 80,
                                        height: 40,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                            gradient: LinearGradient(
                                                colors: <Color>[
                                                  Colors.orange,
                                                  Colors.orange,
                                                  Colors.orange,
                                                  Colors.orange,
                                                  Colors.deepOrange,
                                                  Colors.deepOrange
                                                ],
                                                begin: Alignment.topLeft,
                                                end: Alignment.bottomRight)),
//                                        child: Center(
//                                          child: ,
//                                        ),
                                      ),
                                    )),
                        ),
                      ],
                    ),
                  ),
                );
              });
        } else if (snapshot == null) {
          return Center(
              child: SpinKitThreeBounce(
            color: Colors.blue,
            size: 17,
          ));
        } else if (snapshot.data.length == 0) {
          Center(
              child: Text(
            "No Races Here",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
          ));
        }
        return Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.green,
          ),
        );
      },
    );
  }

  void startRace(Race race) {
    race.is_start = true;
    DBProvider.db.update_race(race);
    setState(() {});
  }
}
