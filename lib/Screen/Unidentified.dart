import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:manager_finish/Database.dart';
import 'package:manager_finish/Models/Event.dart';

import '../Models/Note.dart';
import '../Models/Race.dart';

class Unidentified extends StatefulWidget {
  Event_l event;
  String contain_note;
//  Race race;

  Unidentified(this.event, this.contain_note);

  @override
  _UnidentifiedState createState() =>
      _UnidentifiedState(event, contain_note);
}

class _UnidentifiedState extends State<Unidentified> {
  TimeOfDay _time = TimeOfDay.now();
//  Race race;

  void onTimeChanged(TimeOfDay newTime) {
    setState(() {
      _time = newTime;
    });
  }

/**********************************************************************************/
  String contain_note;
  Event_l event;
  String lapsTimes;
  String lapsValue;
//  List<Race> loops_name = new List();

  @override
  void initState() {
//    DBProvider.db.Race_event_list(event).then((value) {
//      setState(() {
//        loops_name = value;
//      });
//    });
    get_note();
    load_lap();
    super.initState();
  }

//  List<String> lapsList = new List();
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController note_controller = TextEditingController();

  // ignore: non_constant_identifier_names
  get_note() {
//    List<Note>note=new List();
    DBProvider.db.Note_all().then((value) {
      setState(() {
//     print('<<<<<<<<<<<<<<<<< ${value}');
        if (value.length > 0) {
          note_controller.text = value[0].containe;
        }
      });
    });
  }

  load_lap() {
//    setState(() {
////      lapsList = race.js_split["splitNames"]["split"];
//      lapsTimes = lapsList.first;
////        lapsValue = lapsList.first.distance.toString();
//    });
  }

  _UnidentifiedState(this.event, this.contain_note);

  TextEditingController bibController = new TextEditingController();
  String bibNumber;
  DateTime _dateTime;

  Future<void> _showMyDialog() async {
    DateTime temp;
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.black,
          title: Text('Set Time'),
          content: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: TimePickerSpinner(
              normalTextStyle: TextStyle(fontSize: 24, color: Colors.grey),
              highlightedTextStyle:
              TextStyle(fontSize: 24, color: Colors.white),
              isShowSeconds: true,
              is24HourMode: false,
              spacing: 10,
              itemHeight: 60,
              isForce2Digits: true,
              onTimeChange: (time) {
                setState(() {
                  temp = time;
                });
              },
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                if (temp != null) {
                  setState(() {
                    _dateTime = temp;
                  });
                }
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

//

  Widget addBibSheet() {
    return Container(
      height: 200,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(25), topLeft: Radius.circular(25)),
        color: Colors.black,
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 50,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.blueAccent, width: 0.2)),
                child: TextField(
                  keyboardType: TextInputType.number,
                  onChanged: (value) {
                    setState(() {
                      FocusScope.of(_scaffoldKey.currentContext)
                          .requestFocus(null);
//                      FocusScope.of(_scaffoldKey.currentContext).unfocus();
                      bibNumber = value;
                    });
                  },
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w100),
                  textAlign: TextAlign.left,
                  controller: bibController,
                  decoration: new InputDecoration(
                    contentPadding: EdgeInsets.only(
                        left: 8, right: 8.0, bottom: 8.0, top: 10),
                    //border: InputBorder.none,
                    hintStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w100),
                    hintText: 'Din klubb (valfri)',
                  ),
                ),
              ),
            ),
            Container(
              width: 85,
              height: 40,
              decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              child: InkWell(
                onTap: () {
                  print(bibController.text);
                  Navigator.pop(context);
                },
                child: Center(
                  child: Text(
                    "Add",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      floatingActionButton: FloatingActionButton(
//        onPressed: () {},
//      ),
      key: _scaffoldKey,
      appBar: PreferredSize(
        child: Container(
          height: 150,
          child: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Stack(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image:
                                        AssetImage('Images/LaLigaLogo.png'),
                                        fit: BoxFit.fill)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      event.name,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w100),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: Text(
                                        "Unidentified racer",
                                        style: TextStyle(
                                            color: Colors.white54,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w100),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            color: Colors.blue,
                            height: 2,
                          ),
                        )
                      ],
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 24, right: 12.0),
                        child: Container(
                          width: 25,
                          height: 25,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image:
                                  AssetImage('Images/greenProfile.png'))),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(110),
      ),
      backgroundColor: Colors.black,
      body: Container(
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 320,
                      color: Color(0xff121214),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Records a time:",
                                style: TextStyle(
                                    color: Colors.white54,
                                    fontSize: 17,
                                    fontWeight: FontWeight.w100),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 16.0),
                                child: Row(
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        _showMyDialog();
                                      },
                                      child: Text(
                                        "..",
                                        style: TextStyle(
                                            color: Colors.white54,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w100),
                                      ),
                                    ),
                                    Padding(
                                      padding:
                                      const EdgeInsets.only(left: 16.0),
                                      child: Theme(
                                        data: Theme.of(context)
                                            .copyWith(canvasColor: Colors.blue),
                                        child: DropdownButton(
                                          hint: Text(
                                            lapsValue == null
                                                ? "Choose´Lap"
                                                : lapsValue.toString(),
                                            style: TextStyle(
                                                color: Colors.white54,
                                                fontSize: 20,
                                                fontWeight: FontWeight.w100),
                                          ),
                                          onChanged: (value) {
                                            setState(() {
//                                            lapsTimes.distance
                                              print(value);
                                              lapsValue = value;
                                              print(lapsValue);
                                            });
                                          },
//                                          items: List.generate(race.jso[''].length,
//                                                  (index) {
//                                                return DropdownMenuItem(
//                                                  value: lapsList[index],
//                                                  child: Text(
//                                                    '${lapsList[index].toString()}  (${loops_name.firstWhere((element) => element.id == lapsList[index]).id})',
//                                                    style: TextStyle(
//                                                        color: lapsValue != null
//                                                            ? lapsList[index] ==
//                                                            lapsValue
//                                                            ? Colors.black
//                                                            : Colors.white54
//                                                            : Colors.white54,
//                                                        fontSize: 20,
//                                                        fontWeight:
//                                                        FontWeight.w300),
//                                                  ),
//                                                );
//                                              }),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 30.0),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "Bib: ",
                                      style: TextStyle(
                                          color: Colors.white54,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w100),
                                    ),
                                    bibController.text.isEmpty
                                        ? InkWell(
                                      onTap: () {
                                        _scaffoldKey.currentState
                                            .showBottomSheet(
                                                (context) =>
                                                addBibSheet(),
                                            backgroundColor:
                                            Colors.black);
                                      },
                                      child: Text(
                                        "Add",
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w100),
                                      ),
                                    )
                                        : Row(
                                      children: <Widget>[
                                        Container(
                                          width: 200,
                                          child: Text(
                                            bibController.text.toString(),
                                            style: TextStyle(
                                                fontStyle:
                                                FontStyle.italic,
                                                color: Colors.white,
                                                fontSize: 20,
                                                fontWeight:
                                                FontWeight.w100),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                          const EdgeInsets.all(8.0),
                                          child: IconButton(
                                              color: Colors.white,
                                              icon: Icon(Icons.edit),
                                              onPressed: () {
                                                _scaffoldKey.currentState
                                                    .showBottomSheet(
                                                        (context) =>
                                                        addBibSheet(),
                                                    backgroundColor:
                                                    Colors.black);
                                              }),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              contain_note != ''
                                  ? Padding(
                                padding: const EdgeInsets.only(
                                    top: 16, bottom: 8),
                                child: Text("Note:",
                                    style: TextStyle(
                                        color: Colors.white54,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w100)),
                              )
                                  : Container(),
                              contain_note != ''
                                  ? Padding(
                                padding: const EdgeInsets.only(
                                    top: 8, bottom: 8),
                                child: Container(
                                  height: 50,
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.blueAccent,
                                          width: 0.2)),
                                  child: TextField(
                                      onChanged: (va) {
                                        Note note = new Note(
                                            containe:
                                            note_controller.text);
                                        DBProvider.db
                                            .deleteAllNote()
                                            .then((value) => DBProvider.db
                                            .insert_note(note));

                                        setState(() {
//                                      print(note.containe);
                                        });
                                      },
                                      controller: note_controller,
                                      style: TextStyle(
                                          color: Colors.white54,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w100)),
                                ),
                              )
                                  : Container()
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 24.0),
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        width: 250,
                        height: 50,
                        child: Center(
                          child: Text(
                            "CONFIRM",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w300),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 16.0, left: 8.0),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Text(
                    "Back",
                    style: TextStyle(
                        color: Colors.blue,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
/***********************************************************************/
