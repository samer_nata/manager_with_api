import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:manager_finish/Models/Event.dart';


import '../Database.dart';

import '../OurTabBarView/Results.dart';
import '../OurTabBarView/Record.dart';

import '../OurTabBarView/Start.dart';
import '../Models/Race.dart';
import 'package:http/http.dart' as http;

class ControlPage extends StatefulWidget {
  Race race;

  Event_l event_l;

  ControlPage( this.event_l,this.race);

  @override
  _ControlPageState createState() => _ControlPageState( event_l,race);
}

class _ControlPageState extends State<ControlPage>
    with SingleTickerProviderStateMixin {
  Race race;
  TabController tabController;

  Event_l event_l;

  _ControlPageState( this.event_l,this.race);

  @override
  void initState() {
    tabController = new TabController(length: 2, vsync: this);
//    get_information_race();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.black,
      appBar: new AppBar(
        title: Text(
          event_l.name,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w300, fontSize: 18),
        ),
        centerTitle: true,
        leading: Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 8.0),
          child: InkWell(
            onTap: () {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/RacesScreen', (Route<dynamic> route) => false);
            },
            child: Container(
              width: 70,
              height: 50,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('Images/LaLigaLogo.png'),
                      fit: BoxFit.fill)),
            ),
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: 20,
              height: 25,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('Images/greenProfile.png'),
                      fit: BoxFit.scaleDown)),
            ),
          ),
        ],
        backgroundColor: Colors.black,
        bottom: TabBar(
          unselectedLabelColor: Colors.white,
          indicatorColor: Colors.blue,
          labelColor: Colors.blue,
          controller: tabController,
          indicatorWeight: 0.9,
          tabs: <Widget>[
            Text(
              "Start",
              style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18),
            ),
            Text(
              "Tidtagning",
              style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18),
            ),
//            Text(
//              "Resultat",
//              style: TextStyle(fontWeight: FontWeight.w300, fontSize: 18),
//            ),
          ],
        ),
      ),
      body: TabBarView(controller: tabController, children: <Widget>[
        new Container(
          child: Start(event_l),
        ),
        new Container(
          child: Record(event_l,race),
        ),
//        new Container(
//          child:
//          Results(race),
//        ),
      ]),
    );
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }
}
