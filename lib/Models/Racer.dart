class Racer {
  int id;
  String category, club;
  String gender, surname, firstname, bib;
//  int ref_race;

  Racer(
      {this.id,
      this.bib,
      this.firstname,
      this.category,
      this.club,
      this.gender,
      this.surname,
//      this.ref_race,
     });

  factory Racer.fromJSONserver(json) {
    return Racer(
        id: int.parse(json["id"]),
        category: json["category"],
        club: json["club"],
        gender: json["gender"],
        surname: json["surname"],firstname: json["firstname"],
        bib: json["bib"],);
  }

  factory Racer.fromJSON(json) {
    return Racer(
        id: json["id"],
        category: json["category"],
        club: json["club"],
        gender: json["gender"],
        surname: json["surname"],
        bib: json["bib"],
        firstname: json["firstname"],
//        ref_race: json['ref_race']
    );
  }

  Map<String, dynamic> to_Map() => {
        'id': id,
        'category': category,
        'club': club,
        'gender': gender,
        'surname': surname,
        'bib': bib,
        "firstname": firstname,
//        "ref_race": ref_race,
      };

  @override
  String toString() {
    return 'Racer{id: $id, category: $category, club: $club, gender: $gender, surname: $surname, firstname: $firstname, bib: $bib,}';
  }
}
