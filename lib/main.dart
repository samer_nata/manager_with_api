import 'dart:convert';

import 'package:flutter/material.dart';
import 'Screen/ControlPage.dart';
import 'Screen/EventsScreen.dart';
import 'Screen/RacesScreen.dart';

import 'Database.dart';

import 'Screen/SplashScreen.dart';

void main() {
  runApp(MyApp());
  DBProvider.db.database.then((value) => DBProvider.db.open());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var key = GlobalKey();

  /*****samer****/

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      key: key,
      debugShowCheckedModeBanner: false,
      routes: {
        '/RacesScreen': (context) => EventsScreen(),
        '/Control': (context) => ControlPage(
            ModalRoute.of(context).settings.arguments, ModalRoute.of(context).settings.arguments
      ),
        "/loops": (context) =>
            RacesScreen(ModalRoute.of(context).settings.arguments),
      },
      initialRoute: '/',
      home: SplashScreen(),
    );
  }
}
