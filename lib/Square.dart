import 'dart:async';

class Square {
  String txt;
  String bib;
bool is_start_from_before=false;
//  Timer timer_square=new Timer(const Duration(seconds: 1), (){});

  bool is_start = false;
  bool is_start_from_page=false;
  int timerMaxSeconds;

  Square({this.txt, this.bib, this.timerMaxSeconds});

  final interval = const Duration(seconds: 1);

//  final int timerMaxSeconds = 10;

  int currentSeconds = 0;

  startTimeout([int milliseconds]) {
    this.is_start=true;
    this.is_start_from_before=true;
    if(this.currentSeconds!=0){
      this.timerMaxSeconds=this.timerMaxSeconds-this.currentSeconds;
    }
    var duration = this.interval;
    Timer.periodic(duration, (timer) {
//        print(timer.tick);
      if (this.is_start == true) {
        this.currentSeconds = this.timerMaxSeconds - timer.tick;

        if (timer.tick >= this.timerMaxSeconds) {
          timer.cancel();
//          this.currentSeconds=0;
        this.is_start_from_page=false;
        }
      }
    });
  }

  @override
  String toString() {
    return 'Square{txt: $txt, is_start: $is_start, currentSeconds: $currentSeconds}';
  }

  cancel() {
    this.is_start = false;
//    this.is_start_from_page=false;
//   if(this.timer_square.isActive){
//     this.timer_square.cancel();
//   }
  }

  start() {

    this.startTimeout();
  }
}
