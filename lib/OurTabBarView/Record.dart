import 'dart:async';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:manager_finish/Models/Event.dart';
import 'package:manager_finish/Models/Loop.dart';
import 'package:manager_finish/Models/Race.dart';


import 'package:manager_finish/Square.dart';

import '../Database.dart';
import '../Screen/EndRacer.dart';
import '../Screen/Unidentified.dart';

class Record extends StatefulWidget {
  Event_l event_l;

  Race race;

  Record(this.event_l, this.race);

  @override
  _RecordState createState() => _RecordState(event_l, race);
}

class _RecordState extends State<Record> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isAll = false;
  Race race;
  Timer _timer;
  Event_l event_l;
  List<String> lapsList = new List();
  List<Square> list_square_orange = new List();
  List<Square> list_square_blue = new List();
  List<Square> list_square_grey = new List();
  List<Square> list_square_grey_io = new List();
  List orang_list = ["76", "101", "102", "67", "68"];
  List blue_list = ["201", "202", "203"];
  List grey_list = ["O|"];
  List grey_list_io = ["O| 1"];

//  List orang_list_r = ["76F", "101F", "102F", "67F", "68F"];
//  List blue_list_r = ["201", "202", "203"];
//  List grey_list_r = ["O|"];

  load_square_list() {
    list_square_orange.clear();
    list_square_blue.clear();
    list_square_grey.clear();
    list_square_grey_io.clear();
    orang_list.forEach((element) {
      print(element);
      Square square = new Square(bib: "F", txt: element, timerMaxSeconds: 10);
      list_square_orange.add(square);
    });
    blue_list.forEach((element) {
      Square square = new Square(bib: "3/7", txt: element, timerMaxSeconds: 10);
      list_square_blue.add(square);
    });
    grey_list.forEach((element) {
      Square square = new Square(
        bib: "",
        txt: element,
      );
      list_square_grey.add(square);
    });
    grey_list_io.forEach((element) {
      Square square = new Square(bib: "", txt: element);
      list_square_grey_io.add(square);
    });
  }

  stop_all() {
    list_square_orange.forEach((element) {
      if (element.is_start_from_page) {
        element.cancel();
      }
    });
    list_square_blue.forEach((element) {
      if (element.is_start_from_page) {
        element.cancel();
      }
    });
  }

  start_all() {
    list_square_orange.forEach((element) {
      if (element.is_start_from_page && !element.is_start) {
        element.start();
      }
    });
    list_square_blue.forEach((element) {
      if (element.is_start_from_page && !element.is_start) {
        element.start();
      }
    });
  }

//  Race race;
  bool isLoadingSubmitted = false;
  int indexLoop;
  String value_filter = 'Show all';
  TextEditingController cont_time = new TextEditingController();
  List<Loop> loops_name = new List();

  _RecordState(this.event_l, this.race);

  T getRandomElement<T>(List<T> list) {
    final random = new Random();
    var i = random.nextInt(list.length);
    return list[i];
  }

  random_list(List ls) {
    var element = getRandomElement(ls);
    return element;
  }

  sort_all_list() {
    load_square_list();
  }

  getLapsTimes() {
    lapsList.clear();
//    lapsList=event_l.;
//    print(race.jso['splitNames']);
  }

  @override
  void initState() {
//    startTimeout();

    load_square_list();
    getLapsTimes();
    super.initState();
  }

  Widget endRace() {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Color(0xff121214),
                  height: 60,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.blue,
                      ),
                      Text(
                        "End Rac",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Color(0xff121214),
                  height: 100,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Do you want to end the races? You cantundo this.",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 24.0),
                child: InkWell(
                  onTap: () {
//                    endRaceFunc(race);
                  },
                  child: isLoadingSubmitted
                      ? SpinKitThreeBounce(
                          color: Colors.red,
                          size: 17,
                        )
                      : Container(
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          width: 250,
                          height: 50,
                          child: Center(
                            child: Text(
                              "END RACES",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 24.0),
                child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    width: 250,
                    height: 50,
                    child: Center(
                      child: Text(
                        "KEEP RACES ON ",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 16.0, top: 16.0),
          child: Align(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.close,
                color: Color(0xff121214),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void endRaceFunc([Race race]) {
    race.is_start = false;
    DBProvider.db.update_race(race).then((value) => Navigator.pop(context));
    setState(() {});
  }

  final interval = const Duration(seconds: 1);

  final int timerMaxSeconds = 10;

  int currentSeconds = 0;

//  startTimeout([int milliseconds]) {
//    var duration = interval;
//    Timer.periodic(duration, (timer) {
//      setState(() {
//        print(timer.tick);
//        currentSeconds = timerMaxSeconds - timer.tick;
//        cont_time.text = currentSeconds.toString();
//        if (timer.tick >= timerMaxSeconds) timer.cancel();
//      });
//    });
//  }

  @override
  Widget build(BuildContext context) {
    delay();
    double he_padding = MediaQuery.of(context).size.height / 7;
    return Scaffold(
      backgroundColor: Colors.black,
      key: _scaffoldKey,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  top: 16.0, left: 8.0, right: 8.0, bottom: 8.0),
              child: Container(
                color: Color(0xff121214),
                child: ExpansionTile(
                  title: Container(
                    // width: MediaQuery.of(context).size.width,
                    height: 40,
                    color: Color(0xff121214),
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Text(
                            value_filter,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width / 1.1,
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Stack(
                              children: <Widget>[
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      isAll = true;
                                      indexLoop = -1;
                                      value_filter = "Show all";
                                      sort_all_list();
                                    });
                                  },
                                  child: Text(
                                    "Show all",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 17,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ),
                                !isAll
                                    ? Container()
                                    : Padding(
                                        padding: const EdgeInsets.only(
                                            right: 12.0, top: 0.0),
                                        child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Icon(
                                              Icons.done,
                                              color: Colors.white,
                                            )),
                                      )
                              ],
                            ),
                            Column(
                              children: List.generate(lapsList.length, (index) {
                                return Padding(
                                  padding: const EdgeInsets.only(top: 8),
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        indexLoop = index;
                                        isAll = false;
                                        value_filter = lapsList[index];
                                        sort_all_list();
                                      });
                                    },
                                    child: Stack(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Text(
                                              "Man. Lap time ",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 17,
                                                  fontWeight: FontWeight.w300),
                                            ),
                                            Text(
                                              lapsList[index],
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 17,
                                                  fontWeight: FontWeight.w300),
                                            ),
                                            index == lapsList.length - 1
                                                ? Container()
                                                : Text(
                                                    ", ",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  )
                                          ],
                                        ),
                                        index == indexLoop
                                            ? Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 12.0, top: 0.0),
                                                child: Align(
                                                    alignment:
                                                        Alignment.centerRight,
                                                    child: Icon(
                                                      Icons.done,
                                                      color: Colors.white,
                                                    )),
                                              )
                                            : Container(
                                                width: 0.0,
                                                height: 0.0,
                                              )
                                      ],
                                    ),
                                  ),
                                );
                              }),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 16.0),
                              child: InkWell(
                                onTap: () => finishLoop(),
                                child: Text(
                                  "Finish",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 16.0),
                              child: InkWell(
                                onTap: () {
                                  _scaffoldKey.currentState.showBottomSheet(
                                          (context) => endRace(),
                                      backgroundColor: Colors.black);
                                },
                                child: Text(
                                  "End Race",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.start,
                    children: List.generate(list_square_orange.length, (index) {
                      if (list_square_orange[index].is_start_from_before &&
                          list_square_orange[index].currentSeconds == 0) {
                        return Container();
                      }
                      return list_square_orange[index].currentSeconds != 0
                          ? Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Listener(
                                onPointerDown: (details) {
//                          _buttonPressed = true;
//                          _increaseCounterWhilePressed();
                                  _timer = new Timer(Duration(seconds: 3), () {
                                    setState(() {
                                      list_square_orange[index]
                                          .is_start_from_page = true;
                                      print(list_square_orange[index]);
                                      stop_all();
                                    });
//                              Navigator.push(
//                                  context,
//                                  new MaterialPageRoute(
//                                      builder: (context) =>
//                                          EndRacer(race,event_l))).then((value) {
//                                setState(() {
//                                  start_all();
//                                });
//                              });
                                  });
                                },
                                onPointerUp: (details) {
                                  _timer.cancel();
//                            _buttonPressed = false;
                                },
                                child: Container(
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 6),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                list_square_orange[index].txt,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 17,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 3.0),
                                                child: Text(
                                                  list_square_orange[index].bib,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 11,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Text(
                                            '0:00:${list_square_orange[index].currentSeconds > 0 ? list_square_orange[index].currentSeconds.toString() : '......'}',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w100),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  width: 70,
                                  height: 50,
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                          colors: <Color>[
                                            Colors.orange,
                                            Colors.orange,
                                            Colors.orange,
                                            Colors.orange,
                                            Colors.deepOrange,
                                            Colors.deepOrange
                                          ],
                                          begin: Alignment.topLeft,
                                          end: Alignment.bottomRight),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                ),
                              ),
                            )
                          : list_square_orange[index].currentSeconds == 0
                              ? Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Listener(
                                    onPointerDown: (details) {
//                          _buttonPressed = true;
//                          _increaseCounterWhilePressed();
                                      _timer =
                                          new Timer(Duration(seconds: 3), () {
                                        setState(() {
                                          list_square_orange[index]
                                              .is_start_from_page = true;
//                                      print(list_square_orange[index]);
//                                      start_all();
                                          stop_all();
                                        });
//                                        Navigator.push(
//                                            context,
//                                            new MaterialPageRoute(
//                                                builder: (context) => EndRacer(
//                                                    race, event_l))).then(
//                                            (value) {
//                                          setState(() {
//                                            start_all();
//                                          });
//                                        });
                                      });
                                    },
                                    onPointerUp: (details) {
                                      _timer.cancel();
//                            _buttonPressed = false;
                                    },
                                    child: Container(
                                      child: Center(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 6),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    list_square_orange[index]
                                                        .txt,
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 3.0),
                                                    child: Text(
                                                      list_square_orange[index]
                                                          .bib,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 11,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      width: 70,
                                      height: 50,
                                      decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                              colors: <Color>[
                                                Colors.orange,
                                                Colors.orange,
                                                Colors.orange,
                                                Colors.orange,
                                                Colors.deepOrange,
                                                Colors.deepOrange
                                              ],
                                              begin: Alignment.topLeft,
                                              end: Alignment.bottomRight),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                    ),
                                  ),
                                )
                              : Container();
                    })),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, top: 8.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.start,
                    children: List.generate(list_square_blue.length, (index) {
                      if (list_square_blue[index].is_start_from_before &&
                          list_square_blue[index].currentSeconds == 0) {
                        return Container();
                      }
                      return list_square_blue[index].currentSeconds != 0
                          ? Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Listener(
                                  onPointerUp: (da) {
                                    _timer.cancel();
                                  },
                                  onPointerDown: (de) {
                                    _timer =
                                        new Timer(Duration(seconds: 3), () {
                                      setState(() {
                                        list_square_blue[index]
                                            .is_start_from_page = true;
//                                            print(list_square_orange[index]);
                                        stop_all();
                                      });
//                                      Navigator.push(
//                                              context,
//                                              new MaterialPageRoute(
//                                                  builder: (context) =>
//                                                      EndRacer(race, event_l)))
//                                          .then((value) {
//                                        setState(() {
//                                          start_all();
//                                        });
//                                      });
                                    });
                                  },
                                  child: Container(
                                    child: Center(
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 6),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 13),
                                                    child: Text(
                                                      list_square_blue[index]
                                                          .txt,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 17,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 15, left: 5),
                                                    child: Text(
                                                      "${list_square_blue[index].bib}",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 10,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Text(
                                                '0:00:${list_square_blue[index].currentSeconds > 0 ? list_square_blue[index].currentSeconds.toString() : '.....'}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w100),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    width: 70,
                                    height: 60,
                                    decoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                  )),
                            )
                          : list_square_blue[index].currentSeconds == 0
                              ? Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Listener(
                                    onPointerUp: (da) {
                                      _timer.cancel();
                                    },
                                    onPointerDown: (de) {
                                      _timer =
                                          new Timer(Duration(seconds: 3), () {
                                        setState(() {
                                          list_square_blue[index]
                                              .is_start_from_page = true;

                                          stop_all();
                                        });
//                                        Navigator.push(
//                                            context,
//                                            new MaterialPageRoute(
//                                                builder: (context) => EndRacer(
//                                                    race, event_l))).then(
//                                            (value) {
//                                          setState(() {
//                                            start_all();
//                                          });
//                                        });
                                      });
                                    },
                                    child: Container(
                                      child: Center(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              list_square_blue[index].txt,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 17,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              "${list_square_blue[index].bib}",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ],
                                        ),
                                      ),
                                      width: 70,
                                      height: 50,
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                    ),
                                  ),
                                )
                              : Container();
                    })),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 8.0, top: 8.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Wrap(
                    alignment: WrapAlignment.start,
                    runAlignment: WrapAlignment.start,
                    children:
                        List.generate(list_square_grey_io.length, (index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Listener(
                          onPointerUp: (da) {
                            _timer.cancel();
                          },
                          onPointerDown: (de) {
                            _timer = new Timer(Duration(seconds: 3), () {
//                              Navigator.push(
//                                  context,
//                                  new MaterialPageRoute(
//                                      builder: (context) =>
//                                          Unidentified(event_l, '111')));
                            });
                          },
                          child: Container(
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5),
                                    child: Text(
                                      list_square_grey_io[index].txt,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 19,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Text(
                                    "Hold 3 sec",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 13,
                                        fontWeight: FontWeight.w100),
                                  )
                                ],
                              ),
                            ),
                            width: 70,
                            height: 50,
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                          ),
                        ),
                      );
                    })),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 8.0, top: he_padding),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Wrap(
                    alignment: WrapAlignment.start,
                    runAlignment: WrapAlignment.start,
                    children: List.generate(list_square_grey.length, (index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkWell(
                          onTap: () {
//                            Navigator.push(
//                                context,
//                                new MaterialPageRoute(
//                                    builder: (context) =>
//                                        Unidentified(event_l, '')));
                          },
                          child: Container(
                            child: Center(
                              child: Text(
                                list_square_grey[index].txt,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            width: 70,
                            height: 50,
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                          ),
                        ),
                      );
                    })),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void finishLoop() {
    setState(() {
      list_square_blue.clear();
    });
  }

  Future delay() async {
    await new Future.delayed(new Duration(milliseconds: 1500), () {
//      setState(() {});
    });
  }
}
