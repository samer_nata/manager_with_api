

class Event_l {
  int id;
  DateTime startTimeMs;
  String defaultRace;
  DateTime endTimeMs;
  String location;
  String name;

//  JSON races;

  String reference;

  Event_l(
      {this.startTimeMs,
      this.endTimeMs,
      this.defaultRace,
      this.location,
      this.name,
//      this.races,
      this.reference,
      this.id});

  factory Event_l.fromJSON(json) {
    return Event_l(
      id: json["id"],
      startTimeMs: DateTime.fromMicrosecondsSinceEpoch(json["startTimeMs"]),
      name: json["name"],
      defaultRace: json["defaultRace"],
      location: json["location"],
      endTimeMs: DateTime.fromMicrosecondsSinceEpoch(json["endTimeMs"]),
      reference: json['reference'],
//        races: json['races']
    );
  }

  factory Event_l.fromJSONServer(json) {
    return Event_l(
      startTimeMs: DateTime.fromMicrosecondsSinceEpoch(json["startTimeMs"]),
      name: json["name"],
      defaultRace: json["defaultRace"],
      location: json["location"],
      endTimeMs: DateTime.fromMicrosecondsSinceEpoch(json["endTimeMs"]),
      reference: json['reference'],
//        races: json['races']
    );
  }

  Map<String, dynamic> toMap() => {
        "startTimeMs": startTimeMs.microsecondsSinceEpoch,
        "name": name,
        "endTimeMs": endTimeMs.microsecondsSinceEpoch,
        "defaultRace": defaultRace,
        "location": location,
//        "races": races,
        'reference': reference
      };

  @override
  String toString() {
    return 'Event{id: $id, startTimeMs: $startTimeMs, defaultRace: $defaultRace, endTimeMs: $endTimeMs, location: $location, name: $name, reference: $reference}';
  }
}
