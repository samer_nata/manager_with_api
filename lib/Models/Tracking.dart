import 'dart:convert';

class Tracking {
  DateTime startTimeMs;
  DateTime openingTimeMs;
  String status;
  String  reference;
  DateTime gunTimeMs;


//Time startTimeMs;
  Tracking(
      {this.startTimeMs,
      this.reference,
      this.gunTimeMs,
      this.openingTimeMs,
      this.status});

  factory Tracking.fromJSON( json) {
//    jsonDecode(json);
    if(json!=null){
      return Tracking(
        status: json["status"],
        reference: json["reference"],
        gunTimeMs:json["gunTimeMs"]!=null? DateTime.fromMicrosecondsSinceEpoch(json["gunTimeMs"]):null,
        openingTimeMs: DateTime.fromMicrosecondsSinceEpoch(json["openingTimeMs"]),
        startTimeMs: DateTime.fromMicrosecondsSinceEpoch(json["startTimeMs"]),
      );
    }
    return null;


  }

  @override
  String toString() {
    return 'Tracking{startTimeMs: $startTimeMs, openingTimeMs: $openingTimeMs, status: $status, gunTimeMs: $gunTimeMs, reference: $reference}';
  }
}
