import 'dart:collection';
import 'dart:convert';



import 'Tracking.dart';

class Race {
  int id;
  DateTime startTimeMs;
  String name;

  int courseId;

  String reference;
  bool is_start = false;
  int ref_event;
  Tracking tracking;

  Map<String, dynamic> js_tracking = Map();
  dynamic jso;

//Time startTimeMs;
  Race(
      {this.id,
      this.name,
      this.startTimeMs,
      this.courseId,
      this.reference,
      this.is_start,
      this.ref_event,
      this.tracking,
      this.jso,
      this.js_tracking});

  factory Race.fromJSON(Map<String, dynamic> json) {
    return Race(
        id: json["id"],
        ref_event: json["ref_event"],
        name: json["name"],
        tracking: json["tracking"] != null
            ? Tracking.fromJSON(jsonDecode(json['tracking']))
            : null,
        reference: json["reference"],
        courseId: json['courseId'],
        startTimeMs: DateTime.fromMicrosecondsSinceEpoch(json["startTimeMs"]),
        is_start: json["is_start"] == 0 ? false : true,
        jso: jsonDecode(json['jso']),
        js_tracking: jsonDecode(json['tracking'])
//        js_tracking: json["tracking"],

        );
  }

  factory Race.fromJSONSERVER(Map<String, dynamic> json) {
    if (json.containsKey("tracking")) {
      return Race(
          id: json["id"],
          ref_event: json["ref_event"],
          name: json["name"],
//          tracking: json["tracking"] != null ? Tracking.fromJSON(json["tracking"]) : null,
          reference: json["reference"],
          courseId: json['courseId'],
          startTimeMs: DateTime.fromMicrosecondsSinceEpoch(json["startTimeMs"]),

          jso: {
            "splitNames": json["splitNames"],
            "categories": json["categories"]
          },
          tracking: Tracking.fromJSON(json["tracking"]),
          js_tracking: json['tracking']
//          js_tracking: json["tracking"],
          );
    }
    return Race(
        id: json["id"],
        ref_event: json["ref_event"],
        name: json["name"],
//          tracking: json["tracking"] != null ? Tracking.fromJSON(json["tracking"]) : null,
        reference: json["reference"],
        courseId: json['courseId'],
        startTimeMs: DateTime.fromMicrosecondsSinceEpoch(json["startTimeMs"]),

        jso: {
          "splitNames": json["splitNames"],
          "categories": json["categories"]
        },
        js_tracking: null
//          js_tracking: json["tracking"],
        );
  }

  Map<String, dynamic> toMap() {
    return {
      "ref_event": ref_event,
      "id": id,
      "name": name,
      "startTimeMs": startTimeMs.microsecondsSinceEpoch,
      "courseId": courseId,
      "is_start": is_start == false ? 0 : 1,
      "jso": jsonEncode(jso),
      "reference": reference,
      "tracking":  jsonEncode(js_tracking)
    };
  }

  @override
  String toString() {
    return 'Race{id: $id, startTimeMs: $startTimeMs, name: $name, courseId: $courseId, reference: $reference, is_start: $is_start, ref_event: $ref_event, tracking: $tracking, jso: $jso}';
  }
}
