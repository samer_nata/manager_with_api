class Loop {
  int id;
  String name;
  String date_start;
  double total_distance;
  double X, Y;
  int id_race;
  bool state = false;
  bool is_start=false;

  Loop(
      {this.id,
      this.name,
      this.date_start,
      this.total_distance,
      this.X,
      this.Y,
      this.id_race,
      this.state,this.is_start});

  factory Loop.fromJson(json) {
    return Loop(
        id: json["id"],
        name: json["name"].toString(),
        total_distance: json["total_distance"],
        date_start: json["date_start"],
        X: json["X"],
        Y: json["Y"],
        id_race: json["id_race"],
        state: json["state"] == 0 ? false : true,is_start: json['is_start']==0?false:true);
  }

  factory Loop.fromJSONserver(json) {
    return Loop(
      id: int.parse(json["id"]),
      name: json["name"],
      date_start: json["date_start"],
      total_distance: double.parse(json["total_distance"]),
      id_race: int.parse(json["id_race"]),
      X: double.parse(json["X"]),state: false,
      Y: double.parse(json["Y"]),
    );
  }

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "date_start": date_start,
        "total_distance": total_distance,
        "X": X,
        "Y": Y,
        "id_race": id_race,
        "state": state == true ? 1 : 0, "is_start": is_start == true ? 1 : 0,
      };

  @override
  String toString() {
    return 'Loop{id: $id, name: $name, date_start: $date_start, total_distance: $total_distance, X: $X, Y: $Y, id_race: $id_race, state: $state, is_start: $is_start}';
  }
}
