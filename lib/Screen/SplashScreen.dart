import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


import '../Database.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Timer timer;


  @override
  Widget build(BuildContext context) {
    timer = Timer(const Duration(milliseconds: 2500), () async {

      Navigator.of(context).pushNamedAndRemoveUntil(
          '/RacesScreen', (Route<dynamic> route) => false);
    });
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0.0,
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 30.0),
              child: Container(
                width: 300,
                height: 200,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('Images/LaLigaLogo.png'),
                        fit: BoxFit.fill)),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                "Zxpect Studios",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w100),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
//    getarrangments();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
