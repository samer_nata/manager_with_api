import 'dart:convert';
import 'dart:io';


import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

import 'package:flutter/material.dart';


import 'Models/Course.dart';
import 'Models/Event.dart';
import 'Models/LapsTimes.dart';
import 'Models/Loop.dart';
import 'Models/Manager.dart';
import 'Models/Note.dart';
import 'Models/Ra_RAC.dart';
import 'Models/Race.dart';

import 'Models/Racer.dart';
import 'package:http/http.dart' as http;

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "race_manager_new_ap.db");
    return await openDatabase(path, version: 2,
        onCreate: (Database db, int version) async {
      await db.execute("PRAGMA foreign_keys = ON;");
//          await db.execute("COMMIT;");
//      await db.execute(" PRAGMA foreign_keys=on; COMMIT;");
      /* await db.execute(
          "CREATE TABLE Currency (Currency_id	INTEGER PRIMARY KEY AUTOINCREMENT not null ,Currency_name TEXT UNIQUE not null);");*/
//      await db.execute(
//          "CREATE TABLE Race (id int PRIMARY KEY  not null,name TEXT not null,location TEXT not null,date_race TEXT not null,state BOOLEAN,is_start BOOLEAN);");
//      await db.execute(
//          "CREATE TABLE loops( id int PRIMARY KEY not null,name TEXT not null,date_start TEXT not null,total_distance REAL, X  REAL NOT NULL , Y   REAL not null,id_race int not null,state BOOLEAN, is_start Boolean,FOREIGN KEY (id_race) REFERENCES Race(id) on DELETE CASCADE on update CASCADE );");
//      await db.execute(
//          "CREATE TABLE lap_times (id int PRIMARY key not null,distance REAL ,id_loop int not null,FOREIGN KEY (id_loop) REFERENCES loops(id)  on DELETE CASCADE on update CASCADE);");
      await db.execute(
          "CREATE TABLE Manager (id int PRIMARY KEY ,email TEXT ,password TEXT);");

      await db.execute("CREATE TABLE Note (containe TEXT);");
      await db.execute(
          "CREATE TABLE Events (id INTEGER PRIMARY KEY AUTOINCREMENT,startTimeMs INTEGER ,defaultRace TEXT,endTimeMs INTEGER ,location TEXT,name TEXT UNIQUE ,reference TEXT);");
      await db.execute(
          "CREATE TABLE Races (id int PRIMARY KEY,startTimeMs INTEGER ,name TEXT,jso JSON,courseId int,reference TEXT,is_start BOOLEAN,tracking JSON,ref_event int,FOREIGN KEY (ref_event) REFERENCES Events(id)  on DELETE CASCADE on update CASCADE);");
      await db.execute(
          "CREATE TABLE Ra_RAC (firstname  TEXT ,surname TEXT ,id_race int ,FOREIGN KEY (id_race) REFERENCES Racer(id)  on DELETE CASCADE on update CASCADE,UNIQUE(firstname,firstname,id_race));");
      await db.execute(
          "CREATE TABLE Course (id int  PRIMARY KEY,distance REAL,name TEXT,startPos JSON,endPos JSON,js_coursepoints JSON);");
      await db.execute(
          "CREATE TABLE Racer (id int  not null,firstname TEXT ,category TEXT,club TEXT,gender TEXT,surname TEXT,bib TEXT,UNIQUE(firstname,surname));");
    });
  }

  open() async {
    if (_database != null) {
      await openDatabase("race_manager_new_ap.db");
//      await updateall_bill_state_false();
//    await updateall_department_state_false();
//      await   DBProvider.db.close_app();

    }
  }

  Future<void> close() async {
//    await refresh_state_app();
    await _database.close();
  }

/**********************************************************************************/
  Future<void> insert_note(Note note) async {
    await _database.insert(
      'Note',
      note.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> deleteAllNote() async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete("Note");
    return;
  }

  Future<List<Note>> Note_all() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Note');
    return List.generate(maps.length, (i) {
      return Note.fromJSON(maps[i]);
    });
  }

  /*****************************END NOTE******************************************************************/
  /************************************Event*******************************************/

  Future<void> insert_event(Event_l event) async {
    await _database.insert(
      'Events',
      event.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<int> last_insert() async {
    final db = await database;
    List<Map<String, dynamic>> data =
        await db.rawQuery("Select last_insert_rowid() as las");
    return await data[0]["las"] != null
        ? int.parse(data[0]["las"].toString())
        : null;
  }

  Future<void> insertallEvent(List listrace) async {
    final db1 = await database;

    await db1.transaction((db) async {
      listrace.forEach((obj) async {
        Event_l event = new Event_l.fromJSONServer(obj);
        try {
          db.insert('Events', event.toMap(),
              conflictAlgorithm: ConflictAlgorithm.replace);
          last_insert().then((value) {
            if (value != null && listrace[0]['races'] != null) {
              List temp2 = listrace[0]['races'];

              insertallRace(temp2, value);
            }
          });
        } catch (ex) {}
      });
    });
    return;
  }

  Future<List<Event_l>> Events_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Events');
    return List.generate(maps.length, (i) {
      return Event_l.fromJSON(maps[i]);
    });
  }

  Future<void> deleteAllEvent() async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");

    await db.delete("Events");
    return;
  }

  /******************************End Event******************************************************/
  /*************************************Ra_RAC**********************************************/

  Future<void> insert_Ra_RAC(Ra_RAC ra_rac) async {
    await _database.insert(
      'Ra_RAC',
      ra_rac.to_Map(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> deleteAllRa_RAC() async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete("Ra_RAC");
    return;
  }

  Future<void> deleteRa_RAC_from_race(Race race) async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute("DELETE  FROM Ra_RAC where id_race =${race.id}");
    return;
  }

  Future<List<Ra_RAC>> Ra_RAC_all() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Ra_RAC');
    return List.generate(maps.length, (i) {
      return Ra_RAC.fromJSON(maps[i]);
    });
  }

  Future<List<Ra_RAC>> Ra_RAC_from_id_race(Race race) async {
    final db = await database;
    List<Map<String, dynamic>> maps =
        await db.query('Ra_RAC', where: "id_race", whereArgs: [race.id]);
    return List.generate(maps.length, (i) {
      return Ra_RAC.fromJSON(maps[i]);
    });
  }

  /***********************************End_ra_race*********************************************************/
  /**********************Race*****************************************************************************************/

  Future<void> insert_race(Race race) async {
    await _database.insert(
      'Races',
      race.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> update_race(Race race) async {
    await _database.update(
      'Races',
      race.toMap(),
      where: "id = ? ",
      whereArgs: [race.id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
//    print(race.toString());
  }

//  Future<void> update_race(Race race, int id) async {

//    await _database.update(
//      'Races',
//      race.toMap(),
//      where: "id = ?",
//      whereArgs: [id],
//      conflictAlgorithm: ConflictAlgorithm.replace,
//    );
//  }

  Future<void> get_information_race(Race race) async {
    await http
        .get(
            'http://54.77.120.67:8080/rest/races/${race.reference}/startlist?class=App')
        .then((data) {
//      races = [];
      var dataServer = jsonDecode(data.body);

      List racers = dataServer['race']['startlist'];

      if (dataServer.length > 0) {
        DBProvider.db.insertallRacer(racers, race);

      } else if (dataServer.length == 0) {
        DBProvider.db.delete_from_race(race);


      }
    });
  }


  Future<void> get_course_race(Race race) async {
    await http
        .get(
        'http://54.77.120.67:8080/rest/courses/${race.courseId}?class=App')
        .then((data) async {
//      races = [];
      Map<String,dynamic> dataServer = jsonDecode(data.body);

      Map<String,dynamic> course = dataServer;
//print(">>>>>>>>>>>>>>>>>>>>>>> database1");
      if (dataServer != null && course.containsKey('course')) {
        course = dataServer['course'];
       await DBProvider.db.insertallcourses(course, race);

      } else if (dataServer.length == 0) {
////        await    DBProvider.db.delete_from_course(race);


      }
    });
  }

  Future<void> insertallRace(List listrace, int id_event) async {
    final db1 = await database;
//    listrace.forEach((element) {});
    await db1.transaction((db) async {
      listrace.forEach((obj) async {
        Race race = new Race.fromJSONSERVER(obj);
        race.ref_event = id_event;
        race.is_start = false;

        try {
          db.insert('Races', race.toMap(),
              conflictAlgorithm: ConflictAlgorithm.replace);
//          print(">>>>>>>>>>>>>>>>>>>>get_information_race");
          await get_information_race(race);
          await get_course_race(race);
        } catch (ex) {}
      });
    });
    return;
  }

  Future<List<Race>> Race_list_state_true() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      'Races',
    );
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_false(DateTime dateTime) async {
//    safsaf
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      'Races',where: " startTimeMs  > ? ",whereArgs: [dateTime.millisecondsSinceEpoch]
    );
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_all(Event_l event_l) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db
        .query('Races', where: "ref_event = ?", whereArgs: [event_l.id]);
//    print("Races   ${maps}");
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_all_e() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Races');

    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<void> deleteAllRace() async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete(
      "Races",
    );
    return;
  }

  Future<void> deleteAllRace_from_Event(Event_l event_l) async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete("Races", where: "ref_event", whereArgs: [event_l.id]);
    return;
  }

//

  /******************************************end race***************************************************/
  /******************************************COURSE*******************************************************************/

  Future<void> insert_course(Course course) async {
    await _database.insert(
      'Course',
      course.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertallcourses(
      Map<String,dynamic> js,  Race race) async {
    final db1 = await database;
//    await delete_from_course(race);
    db1.transaction((db) async {
      Course course = new Course.fromJSONServer(js);

      print("77777777777777777 C ${course.toString()}");

      try {
        db
            .insert('Course', course.toMap(),
            conflictAlgorithm: ConflictAlgorithm.replace)
            .then((value)  {
//            await insert_Ra_RAC(ra_rac);
//          print("succuess >>>>>>>>>>>> COURES  ${course.toString()}");
        });
      } catch (ex) {}
    });


    return;
  }

  Future<List<Course>> Course_list(Race race) async {
    final db = await database;
    List<Map<String, dynamic>> maps =
        await db.query('Course', where: 'id = ?', whereArgs: [race.courseId],limit: 1);
    return List.generate(maps.length, (i) {
      return Course.fromJSON(maps[i]);
    });
  }

  Future<Course> Course_from_race(Race race) async {
    final db = await database;
    List<Map<String, dynamic>> maps =
    await db.query('Course', where: 'id = ?', whereArgs: [race.courseId],);
print(maps);

    return Course.fromJSON(maps[0]);
  }

  Future<void> delete_from_course(Race race) async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute("DELETE from Course where id = ${race.id}");
    return;
  }

  /****************************************END COURSE******************************************************************************/
  /*****************************Racer **********************************************/
  Future<void> insert_racer(Racer racer) async {
    await _database.insert(
      'Racer',
      racer.to_Map(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertallRacer(List listracer, Race race) async {
    final db1 = await database;
//    await deleteRa_RAC_from_race(race);
    db1.transaction((db) async {
      listracer.forEach((obj) async {
        Racer racer = new Racer.fromJSONserver(obj);

        try {
          db
              .insert('Racer', racer.to_Map(),
                  conflictAlgorithm: ConflictAlgorithm.replace)
              .then((value) async {
            Ra_RAC ra_rac = new Ra_RAC(
                id_race: race.id,
                firstname: racer.firstname,
                surname: racer.surname);
            await insert_Ra_RAC(ra_rac);
          });
        } catch (ex) {}
      });
    });

    return;
  }

  Future<List<Racer>> Racer_list([e]) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Racer');
    return List.generate(maps.length, (i) {
      return Racer.fromJSON(maps[i]);
    });
  }

//  Future<List<Racer>> Racer_list_from_race(Race race) async {
//    final db = await database;
//    List<Map<String, dynamic>> maps1 =
//    await db.rawQuery("SELECT * FROM Ra_RAC ;");
////    print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>R_R  ${maps1}");
//    List<Map<String, dynamic>> maps =
//        await db.rawQuery("SELECT * FROM Racer ;");
////    print("join${maps}");
//    return List.generate(maps.length, (i) {
//      return Racer.fromJSON(maps[i]);
//    });
//  }

  Future<List<Racer>> Racer_list_from_race(Race race) async {
    print(race.toString());
    final db = await database;
//    List<Map<String, dynamic>> maps_ra =
//        await db.rawQuery("SELECT * From  Ra_RAC ;");
//    print(">>>>>>>>>>>>Ra_RAC>>>>>>>>>>>>> ${maps_ra}");

    //
    List<Map<String, dynamic>> mapsFirstname = await db.rawQuery(
        "SELECT * FROM Racer   INNER JOIN Ra_RAC ON  Racer.firstname=Ra_RAC.firstname and Racer.surname=Ra_RAC.surname where Ra_RAC.id_race=${race.id} ; ");

//    List<Map<String, dynamic>> mapSurname = await db.rawQuery(
//        "SELECT * FROM Racer where surname in (SELECT surname FROM  Ra_RAC where id_race = ${race.id} )  ");
//    mapsFirstname.
//    print("join${maps}");
    return List.generate(mapsFirstname.length, (i) {
      return Racer.fromJSON(mapsFirstname[i]);
    });
  }

  Future<List<Racer>> Racer_list_without() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
        await db.query('Racer', groupBy: 'division');
    return List.generate(maps.length, (i) {
      return Racer.fromJSON(maps[i]);
    });
  }

  Future<void> deleteAllRacer() async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");

    await db.delete("Racer");
    return;
  }

  Future<void> delete_from_race(Race race) async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute("DELETE from Ra_RAC where id_race = ${race.id}");
    return;
  }

//  Future<void> delete_Racer(Race race) async {
//    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
//
//    await db
//        .delete("Racer", where: "reference = ?", whereArgs: [race.reference]);
//    return;
//  }

  /***************************End Racer************************************************/
  Future<void> insert_manager(Manager manager) async {
    await _database.insert(
      'Manager',
      manager.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Manager>> Manager_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
        await db.rawQuery("SELECT * FROM Manager");
    print("manager ${maps}");

    return maps != null
        ? List.generate(maps.length, (i) {
            return Manager.fromJSON(maps[i]);
          })
        : null;
  }

  Future<void> deleteAllManager() async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete("Manager");
    return;
  }

  /**********************************end manager ****************************************************/

  /**************************************loops***************************************************************/

  Future<void> insert_loops(Loop loop) async {
    await _database.insert('loops', loop.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace, nullColumnHack: "state");
  }

  insertallLoops(List listrace) async {
    final db1 = await database;

    await db1.transaction((db) async {
      listrace.forEach((obj) async {
        Loop loop = new Loop.fromJSONserver(obj);

        try {
          db.insert('loops', loop.toMap(),
              nullColumnHack: 'state',
              conflictAlgorithm: ConflictAlgorithm.replace);
        } catch (ex) {}
      });
    });
    return;
  }

  Future<List<Race>> Race_event_list(Event_l event) async {
    final db = await database;
//    Race race;
////    race.ref_event
    List<Map<String, dynamic>> maps = await db
        .query('Races', where: "ref_event = ?", whereArgs: [event.reference]);
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Loop>> Loops_list_all() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      'loops',
    );
    return List.generate(maps.length, (i) {
      return Loop.fromJson(maps[i]);
    });
  }

  Future<void> deleteAllLoops() async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete("loops");
    return;
  }

  /******************************************lap_times*********************************************************/
  Future<void> insert_lap_times(LapsTimes lapsTimes) async {
    await _database.insert(
      'lap_times',
      lapsTimes.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  insertall_lapsTimes(List listrace) async {
    final db1 = await database;

    await db1.transaction((db) async {
      listrace.forEach((obj) async {
        LapsTimes lap_times = new LapsTimes.fromJSONserver(obj);

        try {
          db.insert('lap_times', lap_times.toMap(),
              conflictAlgorithm: ConflictAlgorithm.replace);
        } catch (ex) {}
      });
    });
    return;
  }

  Future<List<LapsTimes>> LapsTimes_list(int id_loops) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db
        .query('lap_times', where: "id_loop = ?", whereArgs: [id_loops]);
    return List.generate(maps.length, (i) {
      return LapsTimes.fromJSON(maps[i]);
    });
  }

  Future<List<LapsTimes>> LapsTimes_list_all() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('lap_times');
    return List.generate(maps.length, (i) {
      return LapsTimes.fromJSON(maps[i]);
    });
  }

  Future<List<LapsTimes>> LapsTimes_from_race_list(int id_race) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('lap_times',
        where: "id_loop in (Select id from loops where id_race = ${id_race} )");
    return List.generate(maps.length, (i) {
      return LapsTimes.fromJSON(maps[i]);
    });
  }

  Future<void> deleteAllLapsTimes() async {
    final db = await database;
    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete("lap_times");
    return;
  }
}
