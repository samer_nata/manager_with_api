import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:manager_finish/Models/Event.dart';
import 'package:manager_finish/Models/Manager.dart';
import 'package:manager_finish/Models/Race.dart';
import 'package:manager_finish/Screen/RacesScreen.dart';


import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import '../Screen/ControlPage.dart';
import '../Database.dart';
import '../Link.dart';


class Login extends StatefulWidget {
//  Race race;
Event_l _event_l;
Race race;
  Login(this._event_l,this.race);

  @override
  _LoginState createState() => _LoginState(_event_l,race);
}

class _LoginState extends State<Login> {
  Race race;
  Event_l _event_l;


  _LoginState(this._event_l,this.race);

  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  String email, password;
  bool isLoadingLogin = false;

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Container(
          height: 175,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      '/RacesScreen',
                                      (Route<dynamic> route) => false);
                                },
                                child: Container(
                                  width: 80,
                                  height: 50,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'Images/LaLigaLogo.png'),
                                          fit: BoxFit.fill)),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 12.0),
                              child: Container(
                                //color: Colors.white,
                                width: 200,
                                child: Center(
                                  child: Text(
                                    _event_l.name,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15,
                                        fontWeight: FontWeight.w100),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.blue,
                          height: 2,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(120),
      ),
      backgroundColor: Colors.black,
      body: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: 100,
            color: Color(0xff121214),
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 16.0, right: 16.0, bottom: 8.0),
                    child: TextField(
                      onChanged: (value) {
                        setState(() {
                          email = value;
                        });
                      },
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.w100),
                      textAlign: TextAlign.left,
                      controller: emailController,
                      decoration: new InputDecoration(
                        contentPadding: EdgeInsets.only(
                            left: 8, right: 8.0, bottom: 8.0, top: 10),
                        //border: InputBorder.none,
                        hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.w100),
                        hintText: 'Username',
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 16.0, right: 16.0, bottom: 8.0),
                    child: TextField(
                      onChanged: (value) {
                        setState(() {
                          password = value;
                        });
                      },
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontWeight: FontWeight.w100),
                      textAlign: TextAlign.left,
                      controller: passwordController,
                      decoration: new InputDecoration(
                        contentPadding: EdgeInsets.only(
                            left: 8, right: 8.0, bottom: 8.0, top: 10),
                        //border: InputBorder.none,
                        hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.w100),
                        hintText: 'Password',
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 24.0),
            child: InkWell(
              onTap: () => login(),
              child: isLoadingLogin
                  ? SpinKitThreeBounce(
                      size: 17,
                      color: Colors.blue,
                    )
                  : Container(
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      width: 250,
                      height: 50,
                      child: Center(
                        child: Text(
                          "OK",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                    ),
            ),
          )
        ],
      ),
    );
  }

  Future login() async {
    Navigator.of(context).pushNamedAndRemoveUntil(
      '/RacesScreen', (Route<dynamic> route) => false,);
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) =>
                RacesScreen(_event_l)));

    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) =>
                ControlPage(_event_l,race)));
    String email = emailController.text.trim();
    String password = passwordController.text.trim();
//    Navigator.push(context,
//        new MaterialPageRoute(builder: (context) => new ControlPage(race)));

    if (email.isEmpty && password.isEmpty) {
      Toast.show("Vänligen skriv ditt förnamn , ditt efternamn ", context,
          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
    } else if (email.isEmpty || password.isEmpty) {
      if (email.isEmpty) {
        Toast.show("Vänligen skriv ditt förnamn", context,
            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
      } else if (password.isEmpty) {
        Toast.show("Vänligen skriv ditt efternamn", context,
            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
      }
    } else if (!validateEmail(emailController.text.trim())) {
      Toast.show("Enter validate Email", context,
          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
    } else {
      setState(() {
        isLoadingLogin = true;
      });
      try {
        await http.post('$url/login_manager.php', body: {
          'User_name': email,
          'User_password': password,
        }).then((data) async {
          setState(() {
            isLoadingLogin = false;
          });

          List list = jsonDecode(data.body);

//          if (list as bool == false) {
//            Toast.show("Try again", context,
//                backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
//          }

          if(list[0]['re']=="no found"){
            Toast.show("Try Again !", context,
                backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
          }

         else  {
//            print(list);
            Manager manager = new Manager.fromJSONserver(list[0]);
//            print(manager.toString());
//print(" login>>>>>>>>>>>>>>>>>>>>>>>> ${race.toString()}  ev   ${_event_l.toString()}");
            await DBProvider.db.insert_manager(manager).then((value) {
              Navigator.of(context).pushNamedAndRemoveUntil(
                '/RacesScreen', (Route<dynamic> route) => false,);
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) =>
                          RacesScreen(_event_l)));
if(race!=null){
  Navigator.push(
      context,
      new MaterialPageRoute(
          builder: (context) =>
              ControlPage(_event_l,race)));

}


            });


          }
        });
      } catch (ex) {
        Toast.show("Try Again !", context,
            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
        setState(() {
          isLoadingLogin = false;
        });
        print(ex);
      }
    }
  }
}
