class Ra_RAC {
  int id_race;
String firstname,surname;
  Ra_RAC({this.id_race,this.surname,this.firstname});


  factory Ra_RAC.fromJSON(json) {
    return Ra_RAC(
   id_race: json['id_race'],surname:json['surname'],firstname: json['firstname']);
  }

  Map<String, dynamic> to_Map() => {
     'id_race':id_race,'surname':surname,'firstname':firstname
      };

  @override
  String toString() {
    return 'Ra_RAC{id_race: $id_race, firstname: $firstname, surname: $surname}';
  }
}
