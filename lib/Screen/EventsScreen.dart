import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:http/http.dart' as http;
import 'package:manager_finish/Models/Event.dart';
import 'package:manager_finish/Models/LapsTimes.dart';
import 'package:manager_finish/Models/Loop.dart';
import 'package:manager_finish/Models/Manager.dart';
import 'package:manager_finish/Models/Race.dart';

import '../Database.dart';
import 'RacesScreen.dart';




class EventsScreen extends StatefulWidget {
  @override
  _EventsScreenState createState() => _EventsScreenState();
}

class _EventsScreenState extends State<EventsScreen> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController _scrollController;
  List<Race> races = new List();
  List<Loop> loops = new List();
  List<LapsTimes> lapsTimes = new List();
  List<Race> races_next = new List();
  List<Manager> manager_list = new List();
  Manager manager=new Manager(id: 1,email: "eeeee",password: '123345456788');
//  DateTime time_d=DateTime.fromMicrosecondsSinceEpoch(1599411600000);

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('vill du logga ut?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Annullera'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                DBProvider.db.deleteAllManager().then((value) {
                  manager_list.clear();
                  Navigator.of(context).pop();
                });


              },
            ),
          ],
        );
      },
    );
  }

  get_race_next() {
    DBProvider.db.Race_list_false(DateTime.now()).then((data) {
      print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${data}");
      setState(() {
        races_next = data;
      });
    });
  }

  Widget ourBottomSheet() {
    get_race_next();
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(25), topLeft: Radius.circular(25)),
        color: Colors.blueAccent[200],
      ),
      height: 325,
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 40,
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: races_next.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width / 1.1,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "${races_next[index].name}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w100),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Column(
                                    children: List.generate(
                                        races_next[index].jso['splitNames'].length,
                                        (index2) =>  Text(
                                          races_next[index].jso['splitNames'][index2],
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w100),
                                        )),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "${races_next[index].startTimeMs}",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w100),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16.0, top: 16.0),
            child: Align(
              alignment: Alignment.topRight,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.close,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0, left: 8.0),
              child: Text(
                "Other events : ",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w700),
              ),
            ),
          )
        ],
      ),
    );
  }

  void getarrangments() async {

    await http
        .get(
            'http://54.77.120.67:8080/rest/organizers/laliga/arrangements?class=App')
        .then((data) async {
//      races = [];
      var dataServer = jsonDecode(data.body);
      List temp = dataServer['arrangements'];

//    List temp2 = temp[0]['races'];
//    List temp3= temp2[0]['splitNames'];

//    print(temp);
//    temp.forEach((element) {
////      print(element);
//      Event_l ev=new Event_l.fromJSONServer(element);
//      print(ev.toString());
//    });

//print("temp<<<<<<<<<<<<<<${temp}");
      if (temp!=null&&dataServer.length > 0) {
      await  DBProvider.db.insertallEvent(temp);
        setState(() {});
      } else if (dataServer.length == 0) {
        await   DBProvider.db.deleteAllEvent();
        setState(() {});
      }
    });
  }
get_manager(){
  DBProvider.db.Manager_list().then((value) {

    if ( value!=null&& value.length>0) {
      setState(() {
        manager_list = value;
      });
    }
    if(value==null){
      manager_list.clear();
    }
  });
}
  @override
  void initState() {
    _scrollController = ScrollController();
    manager_list.add(manager);
//    getRaces();
//    get_manager();
    getarrangments();


    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: PreferredSize(
        child: Container(
          height: 150,
          child: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Stack(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image:
                                            AssetImage('Images/LaLigaLogo.png'),
                                        fit: BoxFit.fill)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "La Liga Tävling",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w100),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 16.0),
                                      child: Text(
                                        "Dagens evenemang",
                                        style: TextStyle(
                                            color: Colors.white54,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w100),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: manager_list.length != 0
                                  ? Container(
                                      child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          print(manager_list);
                                          _showMyDialog();
                                          print(manager_list);
//                                      Navigator.push(
//                                          context,
//                                          new MaterialPageRoute(
//                                              builder: (context) => new ControlPage(race)));
                                        });
                                      },
                                      color: Colors.red,
                                      icon: Icon(Icons.power_settings_new),
                                    ))
                                  : null,
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            color: Colors.blue,
                            height: 2,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: Column(
        children: <Widget>[
          Expanded(
            child: racesListView(),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: 25,
                child: InkWell(
                  onTap: () {
                    _scaffoldKey.currentState
                        .showBottomSheet((context) => ourBottomSheet(),
                            backgroundColor: Colors.black)
                        .closed
                        .then((val) {
                      setState(() {});
                    });
                  },
                  child: Stack(
                    children: <Widget>[
                      Text(
                        "Andra evenemang",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w100),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 24.0),
                          child: Icon(
                            Icons.keyboard_arrow_up,
                            color: Colors.blue,
                            size: 30,
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
          )
        ],
      ),
    );
  }

  FutureBuilder<List<Event_l>> racesListView() {
    return FutureBuilder(
      initialData: [],
      future: DBProvider.db.Events_list(),
      builder: (BuildContext context, AsyncSnapshot<List<Event_l>> snapshot) {
        if (snapshot.hasData && snapshot.data.length > 0) {
          return ListView.builder(
              itemCount: snapshot.data.length,
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                Event_l item = snapshot.data[index];
                print(item.toString());
                return Padding(
                  padding: const EdgeInsets.all(1.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => RacesScreen(item))).then(
                          (value) {
//                            DBProvider.db.Manager_list().then((value2) {
//                              print("Navigator : $value2");
//                              if(value2!=null&&value2.length>0){
//                                setState(() {
//                                  manager_list = value2;
//                                });
//                              }
//                              if(value2==null){
//                                manager_list.clear();
//                              }
//                            });
                          });
                    },
                    child: Container(
                      color: Color(0xff121214),
                      height: 90,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Container(
                                    width: 275,
                                    child: Text(
                                      "Name : ${item.name}",
                                      //item.name
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w100),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, top: 2.0),
                                  child: Container(
                                    width: 225,
                                    child: Text(
                                      "Location : ${item.location != null ? item.location : '....'}",
                                      //item.location
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w100),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 16.0),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white70,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              });
        } else if (snapshot == null) {
          return Center(
              child: SpinKitThreeBounce(
            size: 17,
            color: Colors.blue,
          ));
        } else {
          return Center(
              child: Text(
            "No Events Here",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
          ));
        }
//        return Center(
//          child: CircularProgressIndicator(
//            backgroundColor: Colors.green,
//          ),
//        );
      },
    );
  }
}

// we are learning pull with git ....
