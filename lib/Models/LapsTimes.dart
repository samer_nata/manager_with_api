class LapsTimes {
  int id;

  double distance;
  int id_loop;

  LapsTimes({this.id, this.distance, this.id_loop});

  factory LapsTimes.fromJSONserver(json) {
    return LapsTimes(
        id: int.parse(json["id"].toString()),
        distance: double.parse(json["distance"].toString()),
        id_loop: int.parse(json["id_loop"].toString()));
  }



  factory LapsTimes.fromJSON(json) {
    return LapsTimes(
        id: json["id"], distance: json["distance"], id_loop: json["id_loop"]);
  }

  Map<String, dynamic> toMap() => {
        "id": id,
        "distance": distance,
        "id_loop": id_loop,
      };

  @override
  String toString() {
    return 'LapsTimes{id: $id, distance: $distance, id_loop: $id_loop}';
  }
}
