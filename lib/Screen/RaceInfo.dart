import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';





import 'package:http/http.dart' as http;
import 'package:manager_finish/Models/Event.dart';
import 'package:manager_finish/Models/Racer.dart';

import '../Database.dart';
import '../Models/Course.dart';
import '../Models/LapsTimes.dart';
import '../Models/Race.dart';

class RaceInfo extends StatefulWidget {
  Event_l event_l;
  Race race;

  RaceInfo(this.event_l, this.race);

  @override
  _RaceInfoState createState() => _RaceInfoState(event_l, race);
}

class _RaceInfoState extends State<RaceInfo> {
  Event_l event_l;
  Race race;

  _RaceInfoState(this.event_l, this.race);

  List<LapsTimes> lapsList = new List();
  List<Racer> raceList = new List();
  Course course = new Course();
  bool doneRaces = false;
  bool donecourses = false;

  //  get_laps() {
//    DBProvider.db.LapsTimes_list(loop.id).then((data) {
//      setState(() {
//        lapsList = data;
//        print(data);
//      });
//    });
//  }
  Future<void> get_information_race() async {
//    print(race.toString());
    try {
      await http
          .get(
              'http://54.77.120.67:8080/rest/races/${race.reference}/startlist?class=App')
          .then((data) async {
//      races = [];
        var dataServer = jsonDecode(data.body);

        List racers = dataServer['race']['startlist'];

        if (dataServer.length > 0) {
          DBProvider.db.insertallRacer(racers, race);

          setState(() {});
        } else if (dataServer.length == 0) {
          DBProvider.db.delete_from_race(race);
          setState(() {});
        }
      });
    } catch (ex) {
      get_racers();
    }

    get_racers();
  }

  @override
  void dispose() {
    super.dispose();
  } //  void getRacers() async {
//    print(loop.id);
//    try {
//      await http.post('$url/view_racer_in_loop.php', body: {
//        'Id_loop': loop.id.toString(),
//      }).then((data) async {
//        List dataServer = await jsonDecode(data.body);
//        print(dataServer);
//        raceList.clear();
//        dataServer.forEach((element) {
//          Racer racer = Racer.fromJSONserver(element);
//          print(racer.toString());
//          setState(() {
//            raceList.add(racer);
//          });
//        });
//        setState(() {
//          doneRaces = true;
//        });
//        print("/////////////////////////////");
//        print(raceList);
//      });
//    } catch (ex) {
////      Toast.show("Try Again !", context,
////          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
//
//      print(ex);
//    }
//  }

  get_racers() {
    DBProvider.db.Racer_list_from_race(race).then((value) {
      setState(() {
        raceList.clear();
        raceList = value;
        doneRaces = true;
        get_course_race(race);
      });

    });
  }

  Future<void> get_course_race(Race race) async {
    try {
      await http
          .get(
              'http://54.77.120.67:8080/rest/courses/${race.courseId}?class=App')
          .then((data) async {
//      races = [];
        Map<String, dynamic> dataServer = jsonDecode(data.body);

        Map<String, dynamic> course = dataServer;
        print(">>>>>>>>>>>>>>>>>>>>>>> database1");
        if (dataServer != null && course.containsKey('course')) {
          course = dataServer['course'];
          await DBProvider.db.insertallcourses(course, race);

          get_course();
        } else if (dataServer.length == 0) {
//          await DBProvider.db.delete_from_course(race);
          donecourses=true;
//
//          get_course();
        }
      });
    } catch (ex) {
      get_course();
    }
  }

  get_course() {
    DBProvider.db.Course_from_race(race).then((value) {
      setState(() {
        course = value as Course;
        donecourses = true;
        print(value.toString());
      });
    });
  }

  @override
  void initState() {
    get_information_race();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Container(
          height: 150,
          child: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Stack(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image:
                                            AssetImage('Images/LaLigaLogo.png'),
                                        fit: BoxFit.fill)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 16.0, top: 8.0, bottom: 8.0),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      race.name,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w100),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 16.0),
                                      child: Text(
                                        "Info ${race.name}",
                                        style: TextStyle(
                                            color: Colors.white54,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w100),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            color: Colors.blue,
                            height: 2,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 350,
              color: Color(0xff121214),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 3.0),
                        child: Text(
                          "Starting time: ${race.startTimeMs.toString().substring(10, 16)}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w100),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 3.0),
                        child:donecourses&&doneRaces? Text(
                          "Distance: ${course!=null?course.distance!=null?course.distance: '...':'...'}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w100),
                        ):SpinKitThreeBounce( size: 17,
                          color: Colors.blue,)
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 0.0, bottom: 3.0),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: <Widget>[
                              Text(
                                "Mellantider: ",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontWeight: FontWeight.w100),
                              ),
                              course!=null&&donecourses ?  Row(
                               children: List.generate(course.coursepoints.length,
                                       (index) {
                                     return Padding(
                                       padding:
                                       const EdgeInsets.only(left: 4),
                                       child: Text(
                                         '${course.coursepoints[index].distance.toString()} km',
                                         style: TextStyle(
                                             color: Colors.white,
                                             fontSize: 17,
                                             fontWeight: FontWeight.w100),
                                       ),
                                     );
                                   }),
                              ) :Container(),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Text(
                          "Checka in-racers:",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.w100),
                        ),
                      ),
                      doneRaces
                          ? Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, bottom: 3.0),
                              child: raceList.length == 0
                                  ? Padding(
                                      padding: const EdgeInsets.only(top: 16.0),
                                      child: Center(
                                        child: Text(
                                          "No Racers !!",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                              fontWeight: FontWeight.w100),
                                        ),
                                      ),
                                    )
                                  : Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: List.generate(raceList.length,
                                            (index) {
                                          return raceList[index].firstname !=
                                                      null &&
                                                  raceList[index].surname !=
                                                      null
                                              ? Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${raceList[index].firstname.toString()} ${raceList[index].surname.toString()} ",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w100),
                                                  ),
                                                )
                                              : Container();
                                        }),
                                      ),
                                    ),
                            )
                          : raceList.length != 0
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 16.0),
                                  child: Center(
                                    child: Text(
                                      "No Racers !!",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 17,
                                          fontWeight: FontWeight.w100),
                                    ),
                                  ),
                                )
                              : Padding(
                                  padding: EdgeInsets.only(top: 24.0),
                                  child: SpinKitThreeBounce(
                                    size: 17,
                                    color: Colors.blue,
                                  ),
                                )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0, left: 8.0),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Text(
                  "Back",
                  style: TextStyle(
                      color: Colors.blue,
                      fontSize: 15,
                      fontWeight: FontWeight.w300),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
